/**
 * Exercise 8 from chapter 2
 *
 * @author Niccolo Vettorello
 */

public class E8 {
	public static void main(String args[]) {
		int hex = 0xd3;
		int octa = 0256;

		System.out.println("Hex to binary: " + Long.toBinaryString(hex));
		System.out.println("Octa to binary: " + Long.toBinaryString(octa));
	}
}

