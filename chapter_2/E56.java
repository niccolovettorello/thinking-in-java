/**
 * Exercises 5/6 from chapter 2.
 *
 * @author Niccolo Vettorello
 */

class Dog {
	public String name;
	public String says;
}

public class E56 {
	public static void main(String args[]) {
		Dog spot = new Dog();
		Dog scruffy = new Dog();

		spot.name = "Spot";
		spot.says = "Ruff!";

		scruffy.name = "Scruffy";
		scruffy.says = "Wurf!";

		Dog spot2 = new Dog();

		spot2.name = "Spot";
		spot2.says = "Ruff!";

		System.out.println("Equality operator spot-spot2: " + (spot == spot2));
		System.out.println("equals() function spot-spot2: " + (spot.equals(spot2)));

		Dog spot3 = spot;

		System.out.println("Equality operator spot-spot3: " + (spot == spot3));
		System.out.println("equals() function spot-spot3: " + (spot.equals(spot3)));
	}
}

