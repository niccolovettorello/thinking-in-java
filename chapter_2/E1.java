// Static import
import static java.lang.System.out;

/**
 * Write a program that exemplifies the usage of static import.
 *
 * @author Niccolo Vettorello
 */
public class E1 {
	public static void main(String args[]) {
		System.out.println("Explicit reference to container class System");
		out.println("No reference to container class System");
	}
}

