/**
 * Write a program to calculate velocity given constant time and space.
 *
 * @author Niccolo Vettorello
 */
public class E4 {
	public static void main(String args[]) {
		// Space in meters
		double space = 300.0;
		
		// Time in seconds
		double time = 60.0;

		System.out.println("Velocity in m/s = " + (space / time));
	}
}

