/**
 * Demonstrate aliasing with a class containing a float field.
 *
 * @author Niccolo Vettorello
 */
class Aliasing {
	public float f;
}

public class E2 {
	public static void main(String args[]) {
		Aliasing a1 = new Aliasing();
		Aliasing a2 = new Aliasing();

		a1.f = 0.01f;
		a2.f = 1.99f;

		System.out.println("Before assignment:");
		System.out.println("a1 = " + a1.f);
		System.out.println("a2 = " + a2.f);

		a1 = a2;

		System.out.println("After assignment:");
		System.out.println("a1 = " + a1.f);
		System.out.println("a2 = " + a2.f);
	}
}

