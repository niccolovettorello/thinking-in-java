/**
 * Write a program that simulates coin flipping.
 *
 * @author Niccolo Vettorello
 */

import java.util.*;

public class E7 {
	public static void main(String args[]) {
		Random rand = new Random();
		
		for (int i = 0; i < 3; i++) {
			int result = rand.nextInt(2);
			
			System.out.println("You obtained a: " + (result == 0 ? "HEAD" : "TAIL"));
		}
	}
}

