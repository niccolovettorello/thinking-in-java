/**
 * Display float and double largest and smallest number in exponential notation.
 *
 * @author Niccolo Vettorello
 */

public class E9 {
	public static void main(String args[]) {
		System.out.println("Max double: " + Double.MAX_VALUE);
		System.out.println("Min double: " + Double.MIN_VALUE);
		System.out.println("Max float: " + Float.MAX_VALUE);
		System.out.println("Min float: " + Float.MIN_VALUE);
	}
}
