/**
 * Demonstrate aliasing via method call with a class containing a float field.
 *
 * @author Niccolo Vettorello
 */
class Aliasing {
	public float f;

	public static void m(Aliasing a) {
		a.f = 0.0f;
	}
}

public class E3 {
	public static void main(String args[]) {
		Aliasing x = new Aliasing();

		x.f = 0.99f;

		System.out.println("Before assignment:");
		System.out.println("x = " + x.f);

		Aliasing.m(x);

		System.out.println("After assignment:");
		System.out.println("x = " + x.f);
	}
}

