/**
 * Demonstrate the usage of JAVA class fields and reassign them new values.
 *
 * @author Niccolo' Vettorello
 */
public class E5 {
	public static void main(String args[]) {
		DataOnly dataOnly = new DataOnly();

		System.out.println("i = " + dataOnly.i);
		System.out.println("c = " + dataOnly.c + "\n");
		
		dataOnly.i = 100;
		dataOnly.c = 'X';

		System.out.println("i = " + dataOnly.i);
		System.out.println("c = " + dataOnly.c);
	}
}

class DataOnly {
	public int i = 1;
	public char c = 'Y';
}
