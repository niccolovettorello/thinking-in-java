/**
 * Demonstrate the usage of a class default constructor
 *
 * @author Niccolo' Vettorello
 */
public class E3 {
	public static void main(String args[]) {
		DefaultConstructorProof defaultConstructorProof = new DefaultConstructorProof();

		System.out.println("Construction completed!");
	}
}

class DefaultConstructorProof {}
