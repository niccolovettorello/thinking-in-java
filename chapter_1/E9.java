/**
 * Write a program to demonstrate the use of autoboxing.
 *
 * @author Niccolo Vettorello
 */
public class E9 {
	public static void main(String args[]) {
		int x = 1;

		Integer y = x;

		int z = y;

		System.out.println("z = " + z);
	}
}

