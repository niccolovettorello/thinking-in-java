/**
 * Demonstrate that a static field is the same for all instance of a class.
 *
 * @author Niccolo' Vettorello
 */
public class E8 {
	public static void main(String args[]) {
		StaticField sf1 = new StaticField();

		System.out.println("i on sf1 = " + sf1.i);

		sf1.i++;

		StaticField sf2 = new StaticField();

		System.out.println("i on sf2 = " + sf2.i);
	}
}

class StaticField {
	public static int i = 1;
}

