/**
 * Write a program that takes three arguments from the command line and prints them.
 *
 * @author Niccolo Vettorello
 */
public class E10 {
	public static void main(String args[]) {
		System.out.println("Argument 1 = " + args[0]);
		System.out.println("Argument 2 = " + args[1]);
		System.out.println("Argument 3 = " + args[2]);
	}
}

