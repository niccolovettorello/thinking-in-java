import java.util.*;

/**
 * The first Thinking in Java example program.
 * Displays a string and today's date.
 * @author Bruce Eckel
 * @author www.MindView.net
 * @version 4.0
 */

/**
 * <ul>
 * <li> This
 * <li> is
 * <li> a
 * <li> test
 * <li> list
 * </ul>
 */
public class E12 {
	/**
	 * Entry point to class & application.
	 * @param args array of string arguments
	 * @throws exceptions No exception thrown
	 */
	public static void main(String args[]) {
		System.out.println("Hello it's: ");
		System.out.println(new Date());
	}
}

