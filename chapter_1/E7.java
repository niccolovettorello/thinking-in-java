/**
 * Demonstrate the usage of the static keyword.
 *
 * @author Niccolo' Vettorello
 */
public class E7 {
	public static void main(String args[]) {
		System.out.println("i = " + StaticField.i);

		Incrementable.increment();

		System.out.println("i = " + StaticField.i);

		Incrementable.increment();

		System.out.println("i = " + StaticField.i);
	}
}

class Incrementable {
	public static void increment() {
		StaticField.i++;
	}
}

class StaticField {
	public static int i = 1;
}

