/**
 * Demonstrate the usage of JAVA class fields.
 *
 * @author Niccolo' Vettorello
 */
public class E4 {
	public static void main(String args[]) {
		DataOnly dataOnly = new DataOnly();

		dataOnly.i = 100;
		dataOnly.c = 'X';

		System.out.println("Construction completed and fields manipulated!");
	}
}

class DataOnly {
	public int i;
	public char c;
}
