/**
 * Declare an int and a char class fields and prove that JAVA guarantees their initialization
 *
 * @author Niccolo' Vettorello
 */
public class E1 {
	public static void main(String args[]) {
		ExampleClass exampleClass = new ExampleClass();
		
		System.out.println(
			(exampleClass.i == 0) ? "int field has been initialized to 0" :
			"int field has not been initialized"
		);
		
		System.out.println(
			(exampleClass.c == '\u0000') ? "char field has been initialized to the empty character" :
				"char field has not been initialized"
		);
	}
}

class ExampleClass {
	public int i;
	public char c;
}
