/**
 * Demonstrate the usage of methods' arguments.
 *
 * @author Niccolo' Vettorello
 */
public class E6 {
	public static void main(String args[]) {
		ArgumentsProver argumentsProver = new ArgumentsProver();
		
		// 11 characters
		String input = "Test string";

		int result = argumentsProver.storage(input);

		System.out.println("result = " + result);
	}
}

class ArgumentsProver {
	public int storage(String s) {
		return s.length() * 2;
	}
}
